download-data:
  kaggle competitions download -c otto-recommender-system

jsonl2csv INPUT_PATH OUTPUT_PATH:
  cat "{{ INPUT_PATH }}" \
  | jq -c -r '.session as $session | .events[] | . += {"session": $session} | [.aid, .ts, .type, .session] | @csv' \
  > {{ OUTPUT_PATH }}

init-duckdb:
  @echo "Creating data.db..."
  duckdb ./data.db < "select 1;"

prepare-duckdb:
  @echo "Creating useful staging tables..."
  duckdb ./data.db < "create table session_actions (
    session_id bigint primary key not null,
    map varchar
  );"

convert-all-jsonls:
  just jsonl2csv ./otto-recommender-system/train.jsonl ./otto-recommender-system/train.csv
  just jsonl2csv ./otto-recommender-system/test.jsonl ./otto-recommender-system/test.csv

load-csvs:
  duckdb ./data.db < load-csvs.sql

convert-and-load-jsonls: convert-all-jsonls prepare-duckdb load-csvs

run-first-time-setup: download-data init-duckdb convert-and-load-jsonls
