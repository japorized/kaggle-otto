drop table if exists train_events;
drop table if exists test_events;

create table train_events (
  article_id bigint,
  ts bigint,
  action_type bigint,
  session_id bigint
);
create table test_events (
  article_id bigint,
  ts bigint,
  action_type bigint,
  session_id bigint
);

copy train_events from './otto-recommender-system/train.csv';
copy test_events from './otto-recommender-system/test.csv';

create index train_session_idx on train_events(session_id);
create index train_article_idx on train_events(article_id);
create index test_session_idx on test_events(session_id);
create index test_article_idx on test_events(article_id);
