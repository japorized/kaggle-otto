use color_eyre::Result;
use duckdb::types::{FromSql, Value};
use duckdb::{params, Row, ToSql};
use serde::{Deserialize, Serialize};
use std::borrow::BorrowMut;
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
pub struct SessionActions {
    pub clicks: HashMap<i64, i64>,
    pub carts: HashMap<i64, i64>,
    pub orders: HashMap<i64, i64>,
}

impl ToSql for SessionActions {
    fn to_sql(&self) -> duckdb::Result<duckdb::types::ToSqlOutput<'_>> {
        let deserialized_session_actions = serde_json::to_string(self).unwrap();

        Ok(duckdb::types::ToSqlOutput::Owned(Value::Text(
            deserialized_session_actions,
        )))
    }
}

impl FromSql for SessionActions {
    fn column_result(value: duckdb::types::ValueRef<'_>) -> duckdb::types::FromSqlResult<Self> {
        match value {
            duckdb::types::ValueRef::Text(t) => {
                let bytes_as_str = std::str::from_utf8(t)
                    .expect("Could not convert &[u8] to &str after fetching from duckdb");
                let session_actions = serde_json::from_str::<'_, Self>(bytes_as_str)
                    .expect("Could not serialize SessionActions");

                Ok(session_actions)
            }
            _ => panic!("Unrecognized data type for SessionActions"),
        }
    }
}

pub fn session_actions_from_events(events: Vec<Event>) -> HashMap<i64, SessionActions> {
    let mut map: HashMap<i64, SessionActions> = HashMap::new();
    for event in events {
        let item = map.entry(event.session_id).or_insert(SessionActions {
            clicks: HashMap::new(),
            carts: HashMap::new(),
            orders: HashMap::new(),
        });
        let ev_type = match event.action_type {
            EventType::Clicks => item.clicks.borrow_mut(),
            EventType::Carts => item.carts.borrow_mut(),
            EventType::Orders => item.orders.borrow_mut(),
        };

        let ev_type_entry_value = ev_type.entry(event.article_id).or_insert(0);
        *ev_type_entry_value += 1;
    }

    map
}

pub fn write_session_actions(
    map: HashMap<i64, SessionActions>,
    conn: &duckdb::Connection,
) -> Result<()> {
    let mut stmt = conn.prepare("insert into session_actions (session_id, map) values (?, ?)")?;
    for (key, val) in map.into_iter() {
        stmt.execute(params![key, val])?;
    }

    Ok(())
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Event {
    article_id: i64,
    ts: i64,
    action_type: EventType,
    session_id: i64,
}

impl TryFrom<&Row<'_>> for Event {
    type Error = duckdb::Error;

    fn try_from(row: &Row) -> Result<Self, Self::Error> {
        let event_type: String = row.get(2)?;
        println!("{}", event_type);
        let event = Event {
            article_id: row.get(0)?,
            ts: row.get(1)?,
            action_type: EventType::from(event_type.as_str()),
            session_id: row.get(3)?,
        };

        Ok(event)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub enum EventType {
    Clicks,
    Carts,
    Orders,
}

impl From<&str> for EventType {
    fn from(s: &str) -> Self {
        match s {
            "clicks" => EventType::Clicks,
            "carts" => EventType::Carts,
            "orders" => EventType::Orders,
            _ => panic!("Not a recognzied event type"),
        }
    }
}
