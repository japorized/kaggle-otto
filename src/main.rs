mod models;

use crate::models::*;
use color_eyre::Result;
use duckdb::Connection;

fn main() -> Result<()> {
    color_eyre::install()?;

    // let test_data_batch_num = 25;
    let train_data_batch_num = 25;

    let conn = Connection::open("./data.db")?;

    // ensure session_actions is clean
    println!("Clearing train data staging tables...");
    conn.execute("delete from session_actions;", [])?;

    println!("Preparing train data...");
    let mut train_data_stmt =
        conn.prepare(include_str!("paged_train_events_by_unique_session_ids.sql"))?;
    let mut batch_run_num = 0;
    let mut has_exhausted_train_data = false;
    while !has_exhausted_train_data {
        let [offset, limit] = [batch_run_num * train_data_batch_num, train_data_batch_num];
        println!(
            "Preparing train data batch {} (offset {}, limit {})...",
            batch_run_num + 1,
            offset,
            limit
        );
        let events: Vec<Event> = train_data_stmt
            .query_map([offset, limit], |row| Event::try_from(row))?
            .map(|result| result.expect("Could not load train Event"))
            .collect();
        println!("Converting train data to intermediate form...");
        let session_actions = session_actions_from_events(events);
        has_exhausted_train_data = session_actions.len() < train_data_batch_num;
        batch_run_num += 1;

        println!("Saving intermediate form...");
        write_session_actions(session_actions, &conn)?;
    }

    println!("Done preparing train data!");

    println!("Inferring for test data...");
    unimplemented!();
    // let mut test_data_stmt = conn.prepare("select * from test_events limit ?")?;
    // let mut has_exhausted_test_data = false;
    // while !has_exhausted_test_data {
    //     let test_data: Vec<Event> = test_data_stmt
    //         .query_map([test_data_batch_num], |row| Event::try_from(row))?
    //         .map(|result| result.expect("Could not load test Event"))
    //         .collect();

    //     // load the easily-loadable session actions thing as hashsets, in batches
    //     // get intersection of each action and sum em, serving as points
    //     // keep 5 of the highest scores around at all times so that we don't have to load anything
    //     let session_actions = get_session_actions(&session_action_filename)?;
    //     for data in test_data.iter() {
    //         // let (ids, best_click_matches, best_cart_matches, best_order_matches) =
    //         //     session_actions.into_iter().map(|(id, session)| (id, session.clicks));
    //     }

    //     has_exhausted_test_data = test_data.len() < test_data_batch_num;
    // }

    Ok(())
}

// Was trying to use polars at one point, but I didn't have enough compute or RAM for em. This is
// but a vestige of that work, that I thought was interesting to be kept around in case I want to
// give polars another go, which I did retry a couple times.
// fn get_data_schema() -> Schema {
//     Schema::from_iter(vec![
//         Field::new("session", DataType::Int64),
//         Field::new(
//             "events",
//             DataType::List(Box::new(DataType::Struct(vec![
//                 Field::new("aid", DataType::Int64),
//                 Field::new("ts", DataType::Int64),
//                 Field::new("type", DataType::Utf8),
//             ]))),
//         ),
//     ])
// }
