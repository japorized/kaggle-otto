with unique_session_ids as (
    select
        distinct session_id as session_id
    from train_events
    order by session_id
    offset ?
    limit ? -- we can batch stuff by this number here
)
select *
from train_events
where
    session_id in (select session_id from unique_session_ids);
