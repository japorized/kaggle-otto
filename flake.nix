{
  description = "https://www.kaggle.com/competitions/otto-recommender-system";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs { inherit system overlays; };
        rustVersion = pkgs.rust-bin.stable.latest.default;
      in {
        devShell = pkgs.mkShell {
          buildInputs = [
            (rustVersion.override { extensions = [ "rust-src" ]; })
            pkgs.kaggle
            pkgs.just
            pkgs.jq
            pkgs.ripgrep
            pkgs.fd
            pkgs.rust-analyzer
            pkgs.duckdb
          ];

          shellHook = ''
            alias find=fd
          '';
        };
      }
    );
}
