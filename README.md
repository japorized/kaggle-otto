My seriously not-so-serious attempt at a Kaggle competition, in Rust :P

There is no professionalism on this repo. This is a fun event I had with some
friends. If you're looking for professionalism in a fun event with friends, this
isn't the place for it.

Competition page:
https://www.kaggle.com/competitions/otto-recommender-system

---

# Getting Started

```
git clone https://gitlab.com/japorized/otto
cd otto
```

Your dev env should be automatically setup if you have direnv and nix flake.

If not, have a look at the `flake.nix` file and make sure you have all the
necessary dependencies listed in `buildInputs`, and a rust toolchain ofc.


## Prep your data!

Get your kaggle credentials and put them in a `.env` file.

Run ths following once you've setup that up. This might take a while.

```
just run-first-time-setup
```

## Run

```
cargo run .
```

---

# Comments & Disclaimers

## Resource needs

I have no idea how much resource this would need. I had almost 5GB of RAM
available when running it but it still gets killed due to OOM, even if I only
iterate by 1 session ID at a time. So yea, I hadn't been able to run this
thing successfully.

## Strategy for solution

Don't even ask. I don't have sufficient knowledge or context for Multi-Objective
Recommender Systems, which I think is a topic in active research. I'm just
giving Rust a go at a problem some friends and I decided to give a whirl on, and
this is a pretty stupid idea.

## Code quality

This is hackathon-level code. If you're expecting beauty, look elsewhere. And no
I don't have a recommendation for eye bleaches.

Clear disclaimer here: if you've burned your eyes reading this code, you're on
your own. Read at your own risk.

## My takeaways

### `nix flake`

This is my first time using nix flakes in a serious manner (despite the rest of
the repo being not-so-serious). It's pretty good and does exactly what I wanted
it to do — prep my dev env in a reproducible way, so that everything will be
there when I `cd` into the project directory.

### `duckdb`

[DuckDB](https://duckdb.org/) has been really useful when we gave it a try.
There's not a lot of it shown in the code here, but we used the cli a ton to
look at the data after pulling them into a duckdb file. It's fast, and I can
read gigabytes worth of data on a pretty lowly-speced laptop.
